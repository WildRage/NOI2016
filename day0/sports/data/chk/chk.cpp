#include "common.h"

typedef long long lld;

const int N = 5011 * 5011;
const char outputInvalid[] = "Output Invalid!";
char report[1011];
bool finished;
int n, m, ans, stdAns, pool[N * 2 + 1], *equal[N], *vars[N], mods[N], result[N], para[15], *ppool = pool;

void readInput(char* st){
	FILE *fin = fopen(st, "r");
	fscanf(fin, "%d%d", &n, &m);
	for(int i = 1; i <= m; i++){
		equal[i] = ppool;
		ppool += n;
		for(int j = 1; j <= n; j++)
			fscanf(fin, "%d", &equal[i][j]);
		fscanf(fin, "%d%d", &mods[i], &result[i]);
	}
	for(int i = 1; i <= n; i++){
		vars[i] = ppool;
		ppool += m;
	}
	for(int i = 3; i <= 10; i++)
		fscanf(fin, "%d", &para[i]);
	fclose(fin);
}

bool readOutput(char* st){
	FILE *fout = fopen(st, "r");
	if(!fout){
		sprintf(report, "Output File Do Not Exist.\n");
		return false;
	}
	int maxLen = 10 * m;
	for(int i = 1; i <= n; i++){
		char c = fgetc(fout);
		int j = 1;
		for(; j <= maxLen && c >= '0' && c <= '9'; j++, c = fgetc(fout))
			for(int k = 1; k <= m; k++)
				vars[i][k] = (vars[i][k] * 10LL + c - '0') % mods[k];
		for(int k = 1; k <= 5; k++)
			if(c == ' ')
				c = fgetc(fout);
		if(j > maxLen && c >= '0' && c <= '9'){
			sprintf(report, "%sYour output is too long. At line %d.\n", outputInvalid, i);
			return false;
		}
		if(c >= 32){
			sprintf(report, "%sInvalid character '\\%d'. At line %d.\n", outputInvalid, int(c), i);
			return false;
		}
		if(c <= 0 && i != n){
			sprintf(report, "%sOutput file is too short. At line %d.\n", outputInvalid, i);
			return false;
		}
	}
	fclose(fout);
	for(int i = 1; i <= m; i++){
		int sum = 0;
		for(int j = 1; j <= n; j++){
			//printf(" + %d * %d", equal[i][j], vars[j][i]);
			sum = (sum + lld(vars[j][i]) * equal[i][j]) % mods[i];
		}
		//printf(" = %d (mod %d) %d expected %s\n", sum, mods[i], result[i], (sum == result[i] ? "OK" : "NO"));
		ans += (sum == result[i] % mods[i]);
	}
	sprintf(report, "Correct! %d / %d\n", ans, m);
	return true;
}

void readAnswer(char* st){
	FILE *fans = fopen(st, "r");
	fscanf(fans, "%d", &stdAns);
}

void writeResult(){
#ifdef _WIN32
	FILE *fres = fopen("tmp/_eval.score", "w");
#else
	FILE *fres = fopen("/tmp/_eval.score", "w");
#endif
	int score = 0;
	if(finished){
		score = 1;
		if(ans >= 1)
			score = 2;
		for(int i = 3; i <= 10; i++)
			if(stdAns - ans <= para[i])
				score = i;
	}
	fprintf(fres, "%s", report);
	fprintf(fres, "%f\n", double(score));
	fclose(fres);
}

int main(int argc, char** argv){
	static char st[1111];
	readInput(argv[1]);
	readAnswer(argv[3]);
	finished = readOutput(argv[2]);
	writeResult();
	
	return 0;
}
