#include "common.h"

typedef long long lld;

const int N = 5011 * 5011;
const char outputInvalid[] = "Output Invalid!\n";
int n, m, ans, pool[N * 2 + 1], *equal[N], *vars[N], mods[N], result[N], *ppool = pool;

void readInput(char* st){
	FILE *fin = fopen(st, "r");
	fscanf(fin, "%d%d", &n, &m);
	for(int i = 1; i <= m; i++){
		equal[i] = ppool;
		ppool += n;
		for(int j = 1; j <= n; j++)
			fscanf(fin, "%d", &equal[i][j]);
		fscanf(fin, "%d%d", &mods[i], &result[i]);
	}
	for(int i = 1; i <= n; i++){
		vars[i] = ppool;
		ppool += m;
	}
	fclose(fin);
}

bool readOutput(char* st){
	FILE *fout = fopen(st, "r");
	if(!fout){
		printf("Output File Do Not Exist.\n");
		return false;
	}
	int maxLen = 10 * m;
	for(int i = 1; i <= n; i++){
		char c = fgetc(fout);
		int j = 1;
		for(; j <= maxLen && c >= '0' && c <= '9'; j++, c = fgetc(fout))
			for(int k = 1; k <= m; k++)
				vars[i][k] = (vars[i][k] * 10LL + c - '0') % mods[k];
		for(int k = 1; k <= 5; k++)
			if(c == ' ')
				c = fgetc(fout);
		if(j > maxLen && c >= '0' && c <= '9'){
			printf(outputInvalid);
			printf("Your output is too long. At line %d.\n", i);
			return false;
		}
		if(c >= 32){
			printf(outputInvalid);
			printf("Invalid character '\\%d'. At line %d.\n", int(c), i);
			return false;
		}
		if(c <= 0 && i != n){
			printf(outputInvalid);
			printf("Output file is too short. At line %d.\n", i);
			return false;
		}
	}
	fclose(fout);
	for(int i = 1; i <= m; i++){
		int sum = 0;
		for(int j = 1; j <= n; j++){
			//printf(" + %d * %d", equal[i][j], vars[j][i]);
			sum = (sum + lld(vars[j][i]) * equal[i][j]) % mods[i];
		}
		//printf(" = %d (mod %d) %d expected %s\n", sum, mods[i], result[i], (sum == result[i] ? "OK" : "NO"));
		ans += (sum == result[i] % mods[i]);
	}
	return true;
}

int main(int argc, char** argv){
	if(argc <= 1){
		printf("You should use this checker in this way:\n");
		printf("./checker <case_no>\n");
		printf("For example\n");
		printf("./checker 2\n");
		return 0;
	}
	static char st[1111];
	sprintf(st, "sports%s.in", argv[1]);
	readInput(st);
	sprintf(st, "sports%s.out", argv[1]);
	if(readOutput(st))
		printf("Correct! %d / %d\n", ans, m);
	
	return 0;
}
