#include <bits/stdc++.h>
#define ll long long
using namespace std;

ll ans = 0;

int main() {
	freopen("cyclic.in", "r", stdin);
	freopen("cyclic.out", "w", stdout);
	int n, m, k;
	scanf("%d%d%d", &n, &m, &k);
	for (int i = 1; i <= n; ++i)
		for (int j = 1; j <= m; ++j)
			if ((__gcd(j, k) == 1) && (__gcd(i, j) == 1)) {
				//printf("%d %d\n", i, j);
				++ans;
			}
	cout << ans << endl;
	return 0;
}
