#include <bits/stdc++.h>
#define ll long long
#define pb push_back
using namespace std;

int counter;

const int Log = 5, MaxK = 2000;
const int Lim = 20000000, MaxN = Lim + 10, MaxP = 3100000, MaxT = 1000000;
int factor[Log], ftot, prod[1 << Log];
ll ans = 0;

bool notp[MaxN];
int prime[MaxP], ptot;
int miu[MaxN];
int sum[MaxN];
int *dsum[MaxT + 10];

void init(int n) {
	miu[1] = 1;
	for (int i = 2; i <= n; ++i) {
		if (!notp[i]) {
			prime[++ptot] = i;
			miu[i] = -1;
		}
		for (int j = 1; j <= ptot && prime[j] * i <= n; ++j) {
			notp[prime[j] * i] = 1;
			if (i % prime[j] == 0) break;
			miu[prime[j] * i] = -miu[i];
		}
	}
	for (int i = 1; i <= MaxT; ++i) {
		dsum[i] = new int[MaxT / i + 1];
		dsum[i][0] = 0;
		for (int j = 1; j * i <= MaxT; ++j) dsum[i][j] = dsum[i][j - 1] + miu[j * i];
	}
	
	for (int i = 1; i <= n; ++i) sum[i] = sum[i - 1] + miu[i];
}

vector <int> fact[MaxK];
vector <int> get_factor(int x) {
	vector <int> ret;
	for (int i = 1; 1ll * i * i <= x; ++i)
		if (x % i == 0) {
			ret.pb(i);
			if (i * i != x) ret.pb(x / i);
		}
	return ret;
}

ll miu_sum(ll n) {
	return sum[n];
}

map <ll, ll> disc_sum[MaxK];
ll miu_disc_sum(int k, ll n) {
//	for (int i = 1; i <= n; ++i) ret += miu[k * i];
	if (n == 0) return 0;
	if (1ll * k * n <= MaxT) return dsum[k][n];
	if (disc_sum[k].count(n)) return disc_sum[k][n];
	ll ret = 0;
	ret = miu_sum(n);
	vector <int> factor = fact[k];
	for (auto t : factor)
		if (t != 1) ret += miu[t] * miu_disc_sum(t, n / t);
	ret *= miu[k];
	return disc_sum[k][n] = ret;
}

ll get_sum(ll n, int d, int g) {
//	for (int i = 1; i <= n; ++i)
//		if (__gcd(i, d) == g) ret += miu[i];
	ll ret = 0;
	vector <int> factor = fact[d / g];
	for (auto i : factor)
		ret += miu[i] * miu_disc_sum(g * i, n / (g * i));
	return ret;
}
ll calc(ll n, ll m, int d, int g) {
	if (n == 0 || m == 0) return 0;
	ll ret = 0;
	ll lim = min(n, m);
	for (ll l = 1, r = 0; l <= lim; l = r + 1) {
		ll tn = n / l, tm = m / l;
		r = min(n / tn, m / tm);
		ll cnt = get_sum(r, d, g) - get_sum(l - 1, d, g);
		ret += 1ll * cnt * tn * tm;
	}
	return ret;
}

int main() {
	ll n, m;
	int k;
	scanf("%lld%lld%d", &n, &m, &k);
	init(Lim);
	for (int i = 1; i <= k; ++i) 
		if (k % i == 0) fact[i] = get_factor(i);
	for (int i = 2; i * i <= k; ++i)
		if (k % i == 0) {
			factor[ftot++] = i;
			while (k % i == 0) k /= i;
		}
	if (k != 1) factor[ftot++] = k;
	prod[0] = 1;
	for (int s = 1; s < (1 << ftot); ++s) {
		for (int i = 0; i < ftot; ++i)
			if ((s >> i) & 1) {
				prod[s] = prod[s ^ (1 << i)] * factor[i];
				break;
			}
	}

	for (int s = 0; s < (1 << ftot); ++s) {
		ll k;
		if (__builtin_popcount(s) & 1)  k = -1;
		else k = 1;
		ans += k * calc(m / prod[s], n, prod[s], 1); 
		for (int t = s; t; t = (t - 1) & s) {
			ll tmp = k * calc(m / prod[s] * prod[t], n, prod[s], prod[t]);
			ans += tmp;
		}
	}
	cout << ans << endl;
	return 0;
}
