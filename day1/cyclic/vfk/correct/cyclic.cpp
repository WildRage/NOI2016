#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <map>
using namespace std;

typedef long long s64;

const int MaxN = 1000000000;
const int MaxM = 1000000000;
const int MaxK = 2000;

int gcd(int a, int b)
{
	while (b)
	{
		a %= b;
		swap(a, b);
	}
	return a;
}

int n, m, ck;

s64 calc_orz()
{
	s64 cnt = 0;
	for (int y = 1; y <= m; y++)
		if (gcd(y, ck) == 1)
			for (int x = 1; x <= n; x++)
				if (gcd(x, y) == 1)
					cnt++;
	return cnt;
}

const int L = 1000000;
const int MaxF = 40;

int mu[L + 1];
int premu[L + 1];
int presum2[MaxF][L + 1];

int fac_n[MaxK + 1];
int fac[MaxK + 1][MaxF];
int ckid[MaxK + 1];

map<int, s64> bpremu;
map<int, s64> bsum2[MaxK + 1];

s64 query_premu(int N)
{
	if (N == 0)
		return 0;
	if (N <= L)
		return premu[N];
	if (bpremu.count(N))
		return bpremu[N];
	int sum = 1;
	for (int k = 2, nk; k <= N; k = nk)
	{
		nk = N / (N / k) + 1;
		sum -= query_premu(N / k) * (nk - k);
	}
	return bpremu[N] = sum;
}

s64 get_sum2(int tn, int td)
{
	// sum mu[kd]
	
	if (mu[td] == 0)
		return 0;
	if (tn == 0)
		return 0;
	if (td == 1)
		return query_premu(tn);
	if (tn <= L)
		return presum2[ckid[td]][tn];
	if (bsum2[td].count(tn))
		return bsum2[td][tn];

	s64 cur = 0;
	for (int i = 0; i < fac_n[td]; i++)
	{
		int d = fac[td][i];
		if (mu[d] != 0)
			cur += mu[d] * get_sum2(tn / d, d);
	}
	cur *= mu[td];
	return bsum2[td][tn] = cur;
}

s64 get_sum1(int tn, int td, int tg)
{
	// [gcd]
	if (mu[tg] == 0)
		return 0;
	s64 cur = 0;
	int th = td / tg;
	for (int di = 0; di < fac_n[th]; di++)
	{
		int d = fac[th][di];
		cur += mu[d] * get_sum2(tn / d / tg, d * tg);
	}
	return cur;
}

s64 calc()
{
	for (int i = 1; i <= L; i++)
		mu[i] = 0;
	mu[1] = 1;
	for (int i = 1; i <= L; i++)
		for (int j = i + i; j <= L; j += i)
			mu[j] -= mu[i];
	for (int i = 1; i <= L; i++)
		premu[i] = premu[i - 1] + mu[i];

	for (int i = 1; i <= MaxK; i++)
		fac_n[i] = 0;
	for (int i = 1; i <= MaxK; i++)
		for (int j = i; j <= MaxK; j += i)
			fac[j][fac_n[j]++] = i;

	for (int i = 0; i < fac_n[ck]; i++)
	{
		int d = fac[ck][i];
		ckid[d] = i;
		if (mu[d] != 0)
		{
			for (int k = 1; k <= L; k++)
				presum2[i][k] = presum2[i][k - 1] + mu[k] * mu[d] * (gcd(k, d) == 1);
		}
	}

	s64 cnt = 0;
	for (int di = 0; di < fac_n[ck]; di++)
	{
		int d = fac[ck][di];
		if (mu[d] == 0)
			continue;
		for (int gi = 0; gi < fac_n[d]; gi++)
		{
			int g = fac[d][gi];
			int tm = m / (d / g);
			s64 cur = 0;
			for (int c = 1, nc; c <= n && c <= tm; c = nc)
			{
				nc = min(n / (n / c), tm / (tm / c)) + 1;
				cur += (get_sum1(nc - 1, d, g) - get_sum1(c - 1, d, g)) * (n / c) * (tm / c);
			}
			cnt += mu[d] * cur;
		}
	}
	return cnt;
}

int main()
{
	cin >> n >> m >> ck;

	cout << calc() << endl;

	/*
	for (int ti = 0; ti < 100; ti++)
	{
		n = rand() % 100 + 1, m = rand() % 100 + 1, ck = rand() % 100 + 2;
		cout << calc() << endl;
		cout << calc_orz() << endl;
	}
	*/

	return 0;
}
