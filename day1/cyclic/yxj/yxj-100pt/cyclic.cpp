#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>
const int hash_mod = 11229331;
const int limit = (int) 1e7;
using namespace std;

typedef int arr32[(int) 1e7 + 10];

int n, m, k;

arr32 prime, miu, sum_miu;
int pn, p[200010];
int f[55][200010];
int a[1010], b[1010], qn;
int kd[1010], fr[2010], kdn;
int cnt;
int pre_tot_miu[200010];

struct hash {
  int g[11229331];
  int a[200010], next[200010];
  int cnt;
  int get(int x) {
    int *s = g + (x % hash_mod);
    while (*s  &&  a[*s] != x)
      s = next + *s;
    if (*s)
      return *s;
    *s = ++cnt;
    a[cnt] = x;
    return *s;
  }
} h;
void add_keypoint(int x) {
  int t = 1;
  while (t <= x) {
    p[++pn] = t - 1;
    t = x / (x / t) + 1;
  }
  p[++pn] = t - 1;
}
int tot_miu(int x) {
  if (x <= limit)
    return sum_miu[x];
  int ret = 1;
  int t = 2, d;
  while (t <= x) {
    d = x / (x / t) + 1;
    ret -= tot_miu(x / t) * (d - t);
    t = d;
  }
  return ret;
}
int get_f(int x, int d) {
  if (x == 0)
    return 0;
  return f[fr[d]][h.get(x)];
}
void prepare_miu() {
  sum_miu[1] = miu[1] = 1;
  for (int i = 2; i <= limit; ++i) {
    if (prime[i] == 0) {
      prime[++cnt] = i;
      miu[i] = -1;
    }
    sum_miu[i] = sum_miu[i - 1] + miu[i];
    for (int j = 1; j <= cnt; ++j) {
      if (limit / prime[j] < i)
        break;
      prime[i * prime[j]] = true;
      if (i % prime[j] == 0) {
        miu[i * prime[j]] = 0;
        break;
      }
      else  miu[i * prime[j]] = -miu[i];
    }
  }
}
void prepare_f() {
  for (int i = 1; i <= k; ++i)
    if (k % i == 0  &&  miu[i]) {
      kd[++kdn] = i;
      fr[i] = kdn;
      for (int j = 1; j <= pn; ++j) {
        int &u = f[kdn][j];
        u = pre_tot_miu[j];
        for (int k = 2; k <= kdn; ++k) {
          if (i % kd[k] == 0)
            u = u - miu[kd[k]] * get_f(p[j] / kd[k], i);
        }
      }
      for (int j = 1; j <= kdn; ++j) {
        if (i % kd[j] == 0) {
          qn ++;
          a[qn] = i;
          b[qn] = kd[j];
        }
      }
    }
}
long long calc(int l, int r) {
  long long ret = 0;
  for (int i = 1; i <= qn; ++i) {
    int d = a[i], x = b[i];
    ret += (n / r) / (d / x) * miu[d] * miu[x] * (get_f(r / x, d) - get_f((l - 1) / x, d));
  }
  return 1LL * (m / r) * ret;
}
int main() {
	freopen("cyclic.in", "r", stdin);
	freopen("cyclic.out", "w", stdout);
  //freopen("cyclic.in", "r", stdin);
  //freopen("cyclic.ans", "w", stdout);
  
  long long ans = 0;

  cin >> m >> n >> k;

  add_keypoint(n);
  add_keypoint(m);
  sort(p + 1, p + pn + 1);
  pn = unique(p, p + pn + 1) - p - 1;
  prepare_miu();
  for (int i = 1; i <= pn; ++i) {
    h.get(p[i]);
    pre_tot_miu[i] = tot_miu(p[i]);
  }
  prepare_f();
  for (int i = 0; i < pn; ++i)
    ans += calc(p[i] + 1, p[i + 1]);
  cout << ans << endl;
}
