#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>
const int limit = (int) 2e7;
using namespace std;

typedef int arr32[(int) 2e7 + 10];

int n, m, k;

arr32 prime, mu;
int cnt;

void prepare() {
  int num = 0;
  mu[1] = 1;
  for (int i = 2; i <= limit; ++i) {
    if (prime[i] == 0) {
      prime[++cnt] = i;
      mu[i] = -1;
    }
    for (int j = 1; j <= cnt; ++j) {
      if (limit / prime[j] < i)
        break;
      prime[i * prime[j]] = true;
      if (i % prime[j] == 0) {
        mu[i * prime[j]] = 0;
        break;
      }
      else  mu[i * prime[j]] = -mu[i];
    }
  }
}
int gcd(int x, int y) {
  return y ? gcd(y, x % y) : x;
}
int main() {
  //freopen("cyclic.in", "r", stdin);
  //freopen("cyclic2.out", "w", stdout);
  
  long long ans = 0;

  cin >> m >> n >> k;
  prepare();
  for (int j = 1; j <= k; ++j)
      if (k % j == 0  &&  mu[j])
        for (int p = 1; p <= j; ++p) {
          if (j % p == 0)
            for (int i = p; i <= m; i += p)
              if (mu[i]  &&  gcd(j / p, i / p) == 1)
                ans += (long long) mu[i] * (m / i) * mu[j] * ((n / i) / (j / p));
        }
  cout << ans << endl;
}
