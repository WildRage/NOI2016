#include <cstdio>
#include <cstring>
#include <iostream>
#define f(x, y, z) for(int x = (y); x <= (z); ++x)
#define g(x, y, z) for(int x = (y); x < (z); ++x)
#define h(x, y, z) for(int x = (y); x >= (z); --x)

typedef long long LL;

int p1[100086], p2[100086];
struct HS{
	int h1, h2;
};
inline HS operator +(const HS &x, char y){
	return (HS) {(131LL * x.h1 + y) % 998244357, (13131LL * x.h2 + y) % 1000000005};
}

int n;
char s[100086];
HS hs[100086];

inline HS dx(const HS &x, const HS &y, int l){
	HS ret = (HS) {((LL) x.h1 - (LL) y.h1 * p1[l]) % 998244357, ((LL) x.h2 - (LL) y.h2 * p2[l]) % 1000000005};
	if(ret.h1 < 0) ret.h1 += 998244357;
	if(ret.h2 < 0) ret.h2 += 1000000005;
	return ret;
}
inline bool cmp(int e1, int e2, int l){
	HS dx1 = dx(hs[e1], hs[e1 - l], l), dx2 = dx(hs[e2], hs[e2 - l], l);
	return dx1.h1 == dx2.h1 && dx1.h2 == dx2.h2;
}

int main(){
	freopen("excellent.in", "r", stdin);
	freopen("excellent.out", "w", stdout);
	//FILE *con = fopen("con", "w");
	p1[0] = p2[0] = 1;
	f(i, 1, 100000){
		p1[i] = 131LL * p1[i - 1] % 998244357;
		p2[i] = 13131LL * p2[i - 1] % 1000000005;
	}
	int T; scanf("%d", &T);
	while(T--){
		scanf("%s", s + 1);
		n = strlen(s + 1);
		f(i, 1, n) s[i] -= 'a';
		hs[0] = (HS) {1, 1};
		f(i, 1, n) hs[i] = hs[i - 1] + s[i];
		LL ans = 0;
		f(ea, 2, n - 2){
			int na = 0, nb = 0;
			for(int la = 1; (la << 1) <= ea; ++la) if(cmp(ea, ea - la, la)) ++na;
			for(int lb = 1; ea + (lb << 1) <= n; ++lb) if(cmp(ea + lb, ea + (lb << 1), lb)) ++nb;
			//fprintf(con, "ea %d na %d nb %d\n", ea, na, nb);
			ans += (LL) na * nb;
		}
		std::cout << ans << '\n';
	}
	return 0;
}
