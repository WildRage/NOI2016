#include <cstdio>
#define f(x, y, z) for(int x = (y); x <= (z); ++x)

int n, m;
bool av[20008][20008], vis[20008][20008];

void dfs(int i, int j){
	if(!av[i][j] || vis[i][j]) return;
	vis[i][j] = 1;
	dfs(i - 1, j); dfs(i + 1, j); dfs(i, j - 1); dfs(i, j + 1);
}

bool lt(){
	f(i, 1, n) f(j, 1, m) vis[i][j] = 0; 
	f(i, 1, n) f(j, 1, m) if(av[i][j]){
		dfs(i, j); goto out;
	}
	out:;
	f(i, 1, n) f(j, 1, m) if(av[i][j] && !vis[i][j]) return false;
	return true;
}

int main(){
	freopen("grid.in", "r", stdin);
	freopen("grid.out", "w", stdout);
	int T; scanf("%d", &T);
	while(T--){
		int c; scanf("%d%d%d", &n, &m, &c);
		f(i, 1, n) f(j, 1, m) av[i][j] = 1;
		f(_, 1, c){
			int x, y; scanf("%d%d", &x, &y); av[x][y] = 0;
		}
		if(!lt()){
			printf("0\n"); continue;
		}
		if(n * m - c <= 2){
			printf("-1\n"); continue;
		}
		int T2 = 5000000 / n / m;
		f(i, 1, n) f(j, 1, m) if(av[i][j]){
			av[i][j] = 0;
			if(!lt()){
				printf("1\n"); goto end;
			}
			if(!(--T2)) goto out;
			av[i][j] = 1;
		}
		out:;
		printf("2\n");
		end:;
	}
	return 0;
}
