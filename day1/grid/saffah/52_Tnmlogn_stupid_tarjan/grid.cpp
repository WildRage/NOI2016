#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#define f(x, y, z) for(int x = (y); x <= (z); ++x)
#define g(x, y, z) for(int x = (y); x < (z); ++x)
#define MAXN 3007
#define MAXC 3007
typedef long long LL;
// 顺着边界走，区域在左边
struct Point{
	int x, y;
};
inline bool operator <(const Point &a, const Point &b){
	if(a.x != b.x) return a.x < b.x; else return a.y < b.y;
}
inline bool operator ==(const Point &a, const Point &b){
	return a.x == b.x && a.y == b.y;
}
inline bool operator !=(const Point &a, const Point &b){
	return a.x != b.x || a.y != b.y;
}
inline Point operator +(const Point &a, const Point &b){
	return (Point) {a.x + b.x, a.y + b.y};
}
inline Point operator -(const Point &a, const Point &b){
	return (Point) {a.x - b.x, a.y - b.y};
}
inline Point leftTurn(const Point &a){
	return (Point) {-a.y, a.x};
}
Point D[4] = {(Point) {1, 0}, (Point) {0, 1}, (Point) {-1, 0}, (Point) {0, -1}};
Point S[4] = {(Point) {0, 0}, (Point) {-1, 0}, (Point) {-1, -1}, (Point) {0, -1}};
Point E[4] = {(Point) {0, -1}, (Point) {0, 0}, (Point) {-1, 0}, (Point) {-1, -1}};

int n, m;
inline bool valid(const Point &a){
	return a.x >= 0 && a.x <= n + 1 && a.y >= 0 && a.y <= m + 1;
}
Point ob[MAXN * MAXN]; int obn;
int head[MAXN * MAXN], til[MAXN * MAXN * 2], next[MAXN * MAXN * 2]; int en;
Point fi[MAXN * MAXN]; int fin;
int pid(const Point &p, Point *a, int an){
	Point *ptr = std::lower_bound(a, a + an, p);
	if(ptr != a + an && *ptr == p) return ptr - a; else return -1;
}

int low[MAXN * MAXN], dep[MAXN * MAXN]; bool cut[MAXN * MAXN];

inline void dfs_cut(int x, int fa){
	low[x] = dep[x] = dep[fa] + 1;
	int CN = 0;
	for(int c = head[x]; c != -1; c = next[c]){
		int t = til[c];
		// printf("c1 %d : %d -> %d\n", c, x, t);
		if(t == fa) continue;
		if(dep[t]){
			if(dep[t] < low[x]) low[x] = dep[t];
		}else{
			dfs_cut(t, x);
			++CN;
			if(low[t] < low[x]) low[x] = low[t];
		}
	}
	if(x == 0) cut[x] = (CN > 1); else{
		for(int c = head[x]; c != -1; c = next[c]){
			int t = til[c];
			// printf("c2 %d : %d -> %d\n", c, x, t);
			if(dep[t] == dep[x] + 1 && low[t] >= dep[x]){
				cut[x] = 1; break;
			}
		}
	}
}

int main(){
	freopen("grid.in", "r", stdin);
	freopen("grid.out", "w", stdout);
	int T; scanf("%d", &T);
	f(___, 1, T){
		/* input */{
		int c;
		scanf("%d%d%d", &n, &m, &c);
		// printf("%d %d %d\n", n, m, c);
		obn = 0;
		while(c--){
			scanf("%d%d", &ob[obn].x, &ob[obn].y);
			// printf("%d %d\n", ob[obn].x, ob[obn].y);
			++obn;
		}
		std::sort(ob, ob + obn);
		}
		// if(___ == 5) f(i, 1, n){
			// f(j, 1, m) putchar("X."[pid((Point) {i, j}, ob, obn) == -1]);
			// putchar('\n');
		// }
		// printf("read end\n");
		/* final points */{
		fin = 0;
		f(i, 1, n) f(j, 1, m){
			Point p = (Point) {i, j};
			if(pid(p, ob, obn) == -1) fi[fin++] = p;
		}
		std::sort(fi, fi + fin);
		fin = std::unique(fi, fi + fin) - fi;
		}
		// printf("points end\n");
		/* build final graph */{
		en = 0;
		memset(head, 0xff, sizeof(head[0]) * fin);
		g(i, 0, fin) g(dir, 0, 2){
			Point tar = fi[i] + D[dir];
			int j = pid(tar, fi, fin);
			if(valid(tar) && j != -1){
				// printf("add %d %d\n", i, j);
				til[en] = j; next[en] = head[i];
				head[i] = en++;
				til[en] = i; next[en] = head[j];
				head[j] = en++;
			}
		}
		}
		// printf("build end\n");
		/* find cut and get final result */{
		if(!fin) goto nosolution;
		memset(low, 0, sizeof(low[0]) * fin);
		memset(dep, 0, sizeof(dep[0]) * fin);
		memset(cut, 0, sizeof(cut[0]) * fin);
		dfs_cut(0, 0);
		// printf("root %d %d\n", fi->x, fi->y);
		g(i, 0, fin) if(!dep[i]){
			// printf("notc %d %d\n", fi[i].x, fi[i].y);
			goto notconnected;
		}
		g(i, 0, fin) if(cut[i]) goto havecut;
		if(fin <= 2) goto nosolution;
		goto strongconnected;
		}
		notconnected:
			printf("0\n");
			continue;
		nosolution:
			printf("-1\n");
			continue;
		havecut:
			printf("1\n");
			continue;
		strongconnected:
			printf("2\n");
			continue;
	}
	return 0;
}
