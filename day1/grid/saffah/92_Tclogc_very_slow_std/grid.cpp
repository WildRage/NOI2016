#include <cstdio>
#include <ctime>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#define f(x, y, z) for(int x = (y); x <= (z); ++x)
#define g(x, y, z) for(int x = (y); x < (z); ++x)
#define h(x, y, z) for(int x = (y); x >= (z); --x)

void RE(const char *msg){
	printf("%s\n", msg); exit(1);
}

template <class T>
int pos(const T *begin, const T *end, const T &t){
	const T *ptr = std::lower_bound(begin, end, t);
	if(ptr == end || !(*ptr == t)) return -1;
	return ptr - begin;
}

#define maxC 100007

struct Point{
	int x; int y;
};
inline bool operator <(const Point &a, const Point &b){
	if(a.x != b.x) return a.x < b.x; else return a.y < b.y;
}
inline bool operator ==(const Point &a, const Point &b){
	return a.x == b.x && a.y == b.y;
}
inline Point operator +(const Point &a, const Point &b){
	return (Point) {a.x + b.x, a.y + b.y};
}
int rawN; int rawM;
Point rawObs[maxC]; Point *rawObsEnd;
void input(){
	int obsN;
	scanf("%d%d%d", &rawN, &rawM, &obsN);
	rawObsEnd = rawObs;
	while(obsN--){
		scanf("%d%d", &rawObsEnd->x, &rawObsEnd->y);
		++rawObsEnd;
	}
}

int n; int m;
Point obs[maxC * 21]; Point *obsEnd;
	int disX[maxC * 5]; int *disXEnd;
	int disY[maxC * 5]; int *disYEnd;
void discrete(){
	disXEnd = disX; disYEnd = disY;
	*disXEnd++ = 0; *disXEnd++ = 1; *disXEnd++ = 2;
	*disXEnd++ = rawN - 1; *disXEnd++ = rawN; *disXEnd++ = rawN + 1;
	*disYEnd++ = 0; *disYEnd++ = 1; *disYEnd++ = 2;
	*disYEnd++ = rawM - 1; *disYEnd++ = rawM; *disYEnd++ = rawM + 1;
	for(Point *point = rawObs; point != rawObsEnd; ++point){
		if(point->x > 1) *disXEnd++ = point->x - 2;
		if(point->y > 1) *disYEnd++ = point->y - 2;
		*disXEnd++ = point->x - 1; *disYEnd++ = point->y - 1;
		*disXEnd++ = point->x; *disYEnd++ = point->y;
		*disXEnd++ = point->x + 1; *disYEnd++ = point->y + 1;
		if(point->x < rawN) *disXEnd++ = point->x + 2;
		if(point->y < rawM) *disYEnd++ = point->y + 2;
	}
	std::sort(disX, disXEnd); disXEnd = std::unique(disX, disXEnd);
	std::sort(disY, disYEnd); disYEnd = std::unique(disY, disYEnd);
	n = disXEnd - disX - 2; m = disYEnd - disY - 2;
	obsEnd = obs;
	for(Point *point = rawObs; point != rawObsEnd; ++point)
		*obsEnd++ = (Point) {
			pos(disX, disXEnd, point->x), pos(disY, disYEnd, point->y)
		};
	f(j, 0, m + 1){
		*obsEnd++ = (Point) {0, j}; *obsEnd++ = (Point) {n + 1, j};
	}
	f(i, 1, n){
		*obsEnd++ = (Point) {i, 0}; *obsEnd++ = (Point) {i, m + 1};
	}
	std::sort(obs, obsEnd); obsEnd = std::unique(obs, obsEnd);
}

Point obsList0[maxC * 21]; Point *obsList0End;
Point *obsList0Begin[maxC]; Point **obsList0BeginEnd;
	bool obsVisited[maxC * 21];
		const Point D8[8] = {
			(Point) {-1, -1}, (Point) {-1, 0}, (Point) {-1, 1}, (Point) {0, -1},
			(Point) {0, 1}, (Point) {1, -1}, (Point) {1, 0}, (Point) {1, 1}
		};
	void dfsBranches(const Point &point, int obsId){
		if(obsVisited[obsId]) return;
		obsVisited[obsId] = true; *obsList0End++ = point;
		g(dir, 0, 8){
			Point tar = point + D8[dir];
			int tarId = pos(obs, obsEnd, tar);
			if(tarId != -1) dfsBranches(tar, tarId);
		}
	}
void branches(){
	int obsN = obsEnd - obs;
	memset(obsVisited, false, sizeof(*obsVisited) * obsN);
	obsList0End = obsList0;
	obsList0BeginEnd = obsList0Begin;
	g(i, 0, obsN) if(!obsVisited[i]){
		*obsList0BeginEnd = obsList0End;
		dfsBranches(obs[i], i);
		++obsList0BeginEnd;
	}
	*obsList0BeginEnd = obsList0End;
}

struct PointT{
	int x; int y; char t;
};
inline bool operator <(const PointT &a, const PointT &b){
	if(a.x != b.x) return a.x < b.x; else if(a.y != b.y) return a.y < b.y; else return a.t < b.t;
}
inline bool operator ==(const PointT &a, const PointT &b){
	return a.x == b.x && a.y == b.y;
}
PointT fleaList12[maxC * 504]; PointT *fleaList12End;
PointT *fleaList12Begin[maxC]; PointT **fleaList12BeginEnd;
	inline bool isValid(const Point &point){
		return point.x >= 0 && point.x <= n + 1 && point.y >= 0 && point.y <= m + 1;
	}
	const Point D16[16] = {
		(Point) {-2, -2}, (Point) {-2, -1}, (Point) {-2, 0}, (Point) {-2, 1}, (Point) {-2, 2},
		(Point) {-1, -2}, (Point) {-1, 2}, (Point) {0, -2}, (Point) {0, 2}, (Point) {1, -2}, (Point) {1, 2},
		(Point) {2, -2}, (Point) {2, -1}, (Point) {2, 0}, (Point) {2, 1}, (Point) {2, 2}
	};
void getFleasByBranches(){
	int obsBranchesN = obsList0BeginEnd - obsList0Begin;
	fleaList12End = fleaList12;
	fleaList12BeginEnd = fleaList12Begin;
	for(Point **curObsList0Begin = obsList0Begin; curObsList0Begin != obsList0BeginEnd; ++curObsList0Begin){
		*fleaList12BeginEnd = fleaList12End;
		for(Point *curObs = *curObsList0Begin; curObs != curObsList0Begin[1]; ++curObs){
			g(dir, 0, 8){
				Point tar = *curObs + D8[dir];
				if(isValid(tar) && pos(obs, obsEnd, tar) == -1) *fleaList12End++ = (PointT) {tar.x, tar.y, (char) 1};
			}
			g(dir, 0, 16){
				Point tar = *curObs + D16[dir];
				if(isValid(tar) && pos(obs, obsEnd, tar) == -1) *fleaList12End++ = (PointT) {tar.x, tar.y, (char) 2};
			}
		}
		std::sort(*fleaList12BeginEnd, fleaList12End);
		fleaList12End = std::unique(*fleaList12BeginEnd, fleaList12End);
		++fleaList12BeginEnd;
	}
	*fleaList12BeginEnd = fleaList12End;
}

	bool fleaVisited[maxC * 64];
	PointT *curBranchFlea12ListBegin, *curBranchFlea12ListEnd;
		const Point D4[4] = {
			(Point) {-1, 0}, (Point) {0, -1}, (Point) {0, 1}, (Point) {1, 0}
		};
	void dfsFleaBranches(const PointT &point, int fleaId){
		if(fleaVisited[fleaId]) return;
		fleaVisited[fleaId] = true;
		g(dir, 0, 4){
			Point tar = (Point) {point.x, point.y} + D4[dir];
			int tarId = pos(curBranchFlea12ListBegin, curBranchFlea12ListEnd, (PointT) {tar.x, tar.y, 0});
			if(tarId != -1){
				PointT &tarFlea = curBranchFlea12ListBegin[tarId];
				if(tarFlea.t == 1) dfsFleaBranches(tarFlea, tarId);
			}
		}
	}
bool checkFleaConnectivity(){
	for(PointT **curFleaList12Begin = fleaList12Begin; curFleaList12Begin != fleaList12BeginEnd; ++curFleaList12Begin){
		curBranchFlea12ListBegin = *curFleaList12Begin;
		curBranchFlea12ListEnd = curFleaList12Begin[1];
		int curFleaN = curBranchFlea12ListEnd - curBranchFlea12ListBegin;
		memset(fleaVisited, false, sizeof(*fleaVisited) * curFleaN);
		if(curFleaN <= 0) RE("wrong flea number");
		g(i, 0, curFleaN) if(curBranchFlea12ListBegin[i].t == 1){
			dfsFleaBranches(curBranchFlea12ListBegin[i], 0);
			break;
		}
		g(i, 0, curFleaN) if(!fleaVisited[i] && curBranchFlea12ListBegin[i].t == 1) return false;
	}
	return true;
}

struct AdjNode{
	int til; AdjNode *next;
};
AdjNode *head[maxC * 64];
AdjNode adjList[maxC * 256]; AdjNode *adjListEnd;
void buildFinalGraph(){
	std::sort(fleaList12, fleaList12End);
	fleaList12End = std::unique(fleaList12, fleaList12End);
	int fleaN = fleaList12End - fleaList12;
	adjListEnd = adjList;
	memset(head, 0, sizeof(*head) * fleaN);
	g(p, 0, fleaN) g(dir, 0, 2){
		PointT &flea = fleaList12[p];
		PointT tar = (PointT) {flea.x + !dir, flea.y + dir, 0};
		int op = pos(fleaList12, fleaList12End, tar);
		if(op == -1) continue;
		adjListEnd->til = op; adjListEnd->next = head[p]; head[p] = adjListEnd++;
		adjListEnd->til = p; adjListEnd->next = head[op]; head[op] = adjListEnd++;
	}
}

	int low[maxC * 64]; int dep[maxC * 64]; bool cut[maxC * 64];
	void dfs_cut(int x, int fa){
		static int root = -1;
		if(x == fa) root = x;
		low[x] = dep[x] = dep[fa] + 1;
		int CN = 0;
		for(AdjNode *c = head[x]; c; c = c->next){
			int t = c->til;
			if(t == fa) continue;
			if(dep[t]){
				if(dep[t] < low[x]) low[x] = dep[t];
			}else{
				dfs_cut(t, x);
				++CN;
				if(low[t] < low[x]) low[x] = low[t];
			}
		}
		if(x == root) cut[x] = (CN > 1); else{
			for(AdjNode *c = head[x]; c; c = c->next){
				int t = c->til;
				if(dep[t] == dep[x] + 1 && low[t] >= dep[x]){
					cut[x] = 1; break;
				}
			}
		}
	}
bool checkCut(){
	int fleaN = fleaList12End - fleaList12;
	memset(low, 0, sizeof(*low) * fleaN);
	memset(dep, 0, sizeof(*dep) * fleaN);
	memset(cut, 0, sizeof(*cut) * fleaN);
	g(i, 0, fleaN) if(!dep[i]) dfs_cut(i, i);
	g(i, 0, fleaN) if(cut[i] && fleaList12[i].t == 1) return true;
	return false;
}

int main(){
	freopen("grid.in", "r", stdin);
	freopen("grid.out", "w", stdout);
	int T; scanf("%d", &T);
	while(T--){
		input();
		// printf("input finished\n");
		discrete();
		// printf("discrete finished\n");
		branches();
		// printf("branches finished\n");
		getFleasByBranches();
		// printf("getfleas finished\n");
		if(fleaList12End == fleaList12){
			printf("-1\n"); continue;
		}
		if(!checkFleaConnectivity()){
			printf("0\n"); continue;
		}
		// printf("checkconnectivity finished\n");
		buildFinalGraph();
		// printf("buildgraph finished\n");
		if(checkCut()){
			printf("1\n"); continue;
		}
		if(fleaList12End - fleaList12 <= 2) printf("-1\n"); else printf("2\n");
	}
	// printf("%d\n", (int) clock());
	return 0;
}
