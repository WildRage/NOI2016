#include "testlib.h"
#include <algorithm>
#include <stdio.h>
#include <map>
#include <assert.h>

const int MIN_T = 1;
const int MAX_T = 20;

const int MIN_N = 1;
const int MAX_N = 1000000000;

const int MAX_C_SUM = 100000;

int testcase;

int c_sum = 0;

int T;
int n, m, c;

void check_nm() {
	assert(1 <= testcase && testcase <= 25);
	
	if (testcase <= 1) {
		ensuref(1 <= 1LL * n * m && 1LL * n * m <= 4, "1 <= nm <= 4");
	} else if (testcase <= 2) {
		ensuref(1 <= 1LL * n * m && 1LL * n * m <= 8, "1 <= nm <= 8");
	} else if (testcase <= 3) {
		ensuref(1 <= 1LL * n * m && 1LL * n * m <= 15, "1 <= nm <= 15");
	} else if (testcase <= 4) {
		ensuref(1 <= 1LL * n * m && 1LL * n * m <= 30, "1 <= nm <= 30");
	} else if (testcase <= 5) {
		ensuref(1 <= 1LL * n * m && 1LL * n * m <= 100, "1 <= nm <= 100");
	} else if (testcase <= 6) {
		ensuref(1 <= 1LL * n * m && 1LL * n * m <= 300, "1 <= nm <= 300");
	} else if (testcase <= 7) {
		ensuref(1 <= 1LL * n * m && 1LL * n * m <= 1000, "1 <= nm <= 1000");
	} else if (testcase <= 10) {
		ensuref(1 <= 1LL * n * m && 1LL * n * m <= 20000, "1 <= nm <= 20000");
	} else if (testcase <= 11) {
		ensuref(1 <= 1LL * n * m && 1LL * n * m <= 20000, "1 <= nm <= 20000");
	} else if (testcase <= 12) {
		ensuref(1 <= 1LL * n * m && 1LL * n * m <= 100000, "1 <= nm <= 100000");
	} else if (testcase <= 13) {
		ensuref(1 <= 1LL * n * m && 1LL * n * m <= 300000, "1 <= nm <= 300000");
	} else if (testcase <= 14) {
		ensuref(1 <= 1LL * n * m && 1LL * n * m <= 1000000, "1 <= nm <= 10^6");
	} else if (testcase <= 15) {
		ensuref(1 <= 1LL * n * m && 1LL * n * m <= 1000000000, "1 <= nm <= 10^9");
	}
	
	if (11 <= testcase && testcase <= 15) {
		ensuref(1 <= n && n <= 20000 && 1 <= m && m <= 20000, "1 <= n, m <= 20000");
	} else if (testcase == 16) {
		ensuref(1 <= n && n <= 100000 && 1 <= m && m <= 100000, "1 <= n, m <= 100000");
	} else if (17 <= testcase && testcase <= 25) {
		ensuref(1 <= n && n <= 1000000000 && 1 <= m && m <= 1000000000, "1 <= n, m <= 1000000000");
	}
}

void check_c() {
	assert(1 <= testcase && testcase <= 25);
	
	if (testcase <= 7) {
		ensuref(0 <= c && c <= 1LL * n * m, "0 <= c <= nm");
	} else if (testcase <= 8) {
		ensuref(0 <= c && c <= 5, "0 <= c <= 5");
	} else if (testcase <= 9) {
		ensuref(0 <= c && c <= 15, "0 <= c <= 15");
	} else if (testcase <= 10) {
		ensuref(0 <= c && c <= 30, "0 <= c <= 30");
	} else if (testcase <= 15) {
		// no constraints for c
	} else if (testcase <= 16) {
		// no constraints for c
	} else if (testcase <= 17) {
		ensuref(c == 0, "c = 0");
	} else if (testcase <= 18) {
		ensuref(0 <= c && c <= 1, "0 <= c <= 1");
	} else if (testcase <= 19) {
		ensuref(0 <= c && c <= 2, "0 <= c <= 2");
	} else if (testcase <= 20) {
		ensuref(0 <= c && c <= 3, "0 <= c <= 3");
	} else if (testcase <= 21) {
		ensuref(0 <= c && c <= 10, "0 <= c <= 10");
	} else if (testcase <= 22) {
		ensuref(0 <= c && c <= 30, "0 <= c <= 30");
	} else if (testcase <= 23) {
		ensuref(0 <= c && c <= 300, "0 <= c <= 300");
	}
	// no constraints for c in test 24, 25
}

void check_c_sum() {
	assert(1 <= testcase && testcase <= 25);
	
	if (11 <= testcase && testcase <= 15) {
		ensuref(0 <= c_sum && c_sum <= 20000, "0 <= sigma c <= 20000");
	}
	
	if (testcase == 16) {
		ensuref(0 <= c_sum && c_sum <= 100000, "0 <= sigma c <= 100000");
	}
	
	if (testcase == 24) {
		ensuref(0 <= c_sum && c_sum <= 20000, "0 <= sigma c <= 20000");
	}
	
	if (testcase == 25) {
		ensuref(0 <= c_sum && c_sum <= 100000, "0 <= sigma c <= 100000");
	}
}

int main(int argc, char **argv) {
	
	if (argc - 1 != 1) {
		fprintf(stderr, "usage: %s <test_case_id>\n", argv[0]);
		return 1;
	}
	
	registerValidation();
	
	ensuref(sscanf(argv[1], "%d", &testcase) == 1, " ");
	ensuref(1 <= testcase && testcase <= 25, " ");
	
	T = inf.readInt(MIN_T, MAX_T, "T");
	inf.readEoln();
	
	for (int i = 1; i <= T; i++) {
		n = inf.readInt(MIN_N, MAX_N, "n");
		inf.readSpace();
		m = inf.readInt(MIN_N, MAX_N, "m");
		inf.readSpace();
		
		check_nm();
		
		int tmp = std::min((long long)MAX_C_SUM, 1LL * n * m);
		c = inf.readInt(0, tmp, "c");
		inf.readEoln();
		c_sum += c;
		ensuref(c_sum <= MAX_C_SUM, "sigma c must <= 1e5");
		
		check_c();
		
		std::map<long long, int> M;
		
		for (int i = 1; i <= c; i++) {
			int x, y;
			x = inf.readInt(1, n, "x");
			inf.readSpace();
			y = inf.readInt(1, m, "y");
			inf.readEoln();
			
			long long id = 1LL * (x - 1) * m + y;
			ensuref(M[id] == 0, "one gryllus must be only described once");
			M[id] = 1;
		}
	}
	
	check_c_sum();
	
	inf.readEof();
	
	return 0;
}
