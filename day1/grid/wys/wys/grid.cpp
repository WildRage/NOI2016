#include <stdio.h>
#include <string.h>
#include <algorithm>

const int MAXN = 100005;
const int MAXV = MAXN * 24 * 5;

template <class T1, class T2, int SIZE = 24 * MAXN, int MOD = 1000003>
struct HashMap {
	HashMap() {
		size = 0;
	}
	~HashMap() {}
	
	T1 key[SIZE];
	T2 val[SIZE];
	int next[SIZE];
	int size;
	
	int head[MOD];
	
	T2 & operator [](const T1 &x) {
		int tmp = x % MOD;
		int p = head[tmp];
		while (p) {
			if (key[p] == x) {
				return val[p];
			} else {
				p = next[p];
			}
		}
		next[++size] = head[tmp];
		key[size] = x;
		head[tmp] = size;
		val[size] = 0;
		return val[size];
	}
	
	T2 get(const T1 &x) {
		int tmp = x % MOD;
		int p = head[tmp];
		while (p) {
			if (key[p] == x) {
				return val[p];
			} else {
				p = next[p];
			}
		}
		return 0;
	}
	
	void del(const T1 &x) {
		int tmp = x % MOD;
		int p = head[tmp];
		if (key[p] == x) {
			head[tmp] = next[p];
			return;
		}
		int last_p = p;
		p = next[p];
		while (p) {
			if (key[p] == x) {
				next[last_p] = next[p];
				return;
			} else {
				last_p = p;
				p = next[p];
			}
		}
	}
	
	void clear() {
		size = 0;
	}
};

int n, m, c;
int X[MAXN], Y[MAXN];
long long ID[MAXN];

bool gryllus_visited[MAXN];

HashMap<long long, int, MAXN> H;
HashMap<long long, int, MAXV> H_flea;

inline long long conv_xy_to_id(int x, int y) {
	return 1LL * x * m + y;
}

bool has_cut_point;
bool has_not_cc;

void solve_flea_cc(const int *list, int N);

void solve_gryllus_cc(int id) {
	if (has_not_cc) return;
	
	static int list[MAXN];
	int s = 0, t = 1;
	list[1] = id;
	gryllus_visited[id] = 1;
	while (s < t) {
		int id = list[++s];
		/*if (id < c) {
			int tmp = id + 1;
			gryllus_visited[tmp] = 1;
			list[++t] = tmp;
		}
		continue;
		*/
		int x = X[id], y = Y[id];
		for (int i = x - 2; i <= x + 2; i++) {
			for (int j = y - 2; j <= y + 2; j++) {
				if (i <= 0 || i > n || j <= 0 || j > m) continue;
				if (i == x && j == y) continue;
				long long id1 = conv_xy_to_id(i, j);
				int tmp = H.get(id1);
				if (tmp && !gryllus_visited[tmp]) {
					gryllus_visited[tmp] = 1;
					list[++t] = tmp;
				}
			}
		}
	}
	
	solve_flea_cc(list, t);
}

int flea_X[MAXV], flea_Y[MAXV];
int V;

struct edge {
	edge *next;
	int y;
};

edge *first[MAXV], e[MAXV * 4], *etot;

void clear_edges() {
	etot = e;
	memset(first + 1, 0, V * sizeof(edge*));
}

inline void add_edge(int x, int y) {
	// printf("ade %d %d\n", x, y);
	*++etot = (edge){first[x], y};
	first[x] = etot;
}

void solve_graph();

void solve_flea_cc(const int *list, int N) {
	V = 0;
	
	for (int i = 1; i <= N; i++) {
		int id = list[i];
		int x = X[id], y = Y[id];
		for (int i = x - 2; i <= x + 2; i++) {
			for (int j = y - 2; j <= y + 2; j++) {
				if (i <= 0 || i > n || j <= 0 || j > m) continue;
				if (i == x && j == y) continue;
				long long id1 = conv_xy_to_id(i, j);
				if (H.get(id1)) continue;
				
				int &tmp = H_flea[id1];
				if (tmp) continue;
				
				tmp = ++V;
				flea_X[V] = i;
				flea_Y[V] = j;
				
				// printf("assign %d   %d %d\n", V, i, j);
			}
		}
	}
	/*for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= m; j++) {
			long long id1 = conv_xy_to_id(i, j);
			if (H.get(id1)) continue;
			
			int &tmp = H_flea[id1];
			if (tmp) continue;
			
			tmp = ++V;
			flea_X[V] = i;
			flea_Y[V] = j;
		}
	}*/
	
	clear_edges();
	
	for (int i = 1; i <= V; i++) {
		int x = flea_X[i], y = flea_Y[i];
		long long id = conv_xy_to_id(x, y);
		// printf("flea %d, pos (%d, %d)\n", i, x, y);
		if (x > 1) {
			int tmp = H_flea.get(id - m);
			if (tmp) {
				add_edge(i, tmp);
				add_edge(tmp, i);
			}
		}
		if (y > 1) {
			int tmp = H_flea.get(id - 1);
			if (tmp) {
				add_edge(i, tmp);
				add_edge(tmp, i);
			}
		}
	}
	
	for (int i = 1; i <= V; i++) {
		int x = flea_X[i], y = flea_Y[i];
		long long id = conv_xy_to_id(x, y);
		H_flea.del(id);
	}
	H_flea.clear();
	
	solve_graph();
}

bool visit[MAXV];
int dfn[MAXV];
int low[MAXV];
bool instack[MAXV];
int dfn_tot;

void dfs(int x, int last) {
	dfn[x] = low[x] = ++dfn_tot;
	instack[x] = 1;
	visit[x] = 1;
	static int cnt_1;
	if (x == 1) cnt_1 = 0;
	for (edge *e = first[x]; e; e = e->next) {
		int y = e->y;
		if (!visit[y]) {
			if (x == 1) ++cnt_1;
			dfs(y, x);
			low[x] = std::min(low[x], low[y]);
			
			if (x != 1 && low[x] >= dfn[x]) {
				has_cut_point = 1;
			}
		} else if (y != last && instack[y]) {
			low[x] = std::min(low[x], dfn[y]);
		}
	}
	if (x != 1 && low[x] >= dfn[x]) {
		has_cut_point = 1;
	}
	if (x == 1 && cnt_1 > 1) {
		has_cut_point = 1;
	}
	instack[x] = 0;
}

void solve_graph() {
	memset(visit + 1, 0, V * sizeof(bool));
	dfn_tot = 0;
	dfs(1, 0);
	
	for (int i = 2; i <= V; i++) {
		if (!visit[i]) {
			has_not_cc = true;
			return;
		}
	}
}

int solve() {
	scanf("%d%d%d", &n, &m, &c);
	
	if (c == 0) {
		if (1LL * n * m <= 2) {
			return -1;
		} else if (n == 1 || m == 1) {
			return 1;
		} else {
			return 2;
		}
	}
	
	for (int i = 1; i <= c; i++) {
		scanf("%d%d", X + i, Y + i);  // [1, n]
	}
	
	if (1LL * n * m - c <= 1) {
		return -1;
	}
	
	has_cut_point = false;
	has_not_cc = false;
	
	memset(gryllus_visited + 1, 0, c * sizeof(int));
	
	for (int i = 1; i <= c; i++) {
		ID[i] = conv_xy_to_id(X[i], Y[i]);
		H[ID[i]] = i;
	}
	
	for (int i = 1; i <= c; i++) {
		if (!gryllus_visited[i]) {
			solve_gryllus_cc(i);
		}
	}
	
	for (int i = 1; i <= c; i++) {
		H.del(ID[i]);
	}
	H.clear();
	
	// printf("has not cc: %d   has cp %d\n", has_not_cc, has_cut_point);
	
	if (has_not_cc) {
		return 0;
	} else if (has_cut_point && 1LL * n * m - c >= 3) {
		return 1;
	} else if (1LL * n * m - c - 2 >= 2) {
		return 2;
	} else {
		return -1;
	}
}

int main() {
	freopen("grid.in", "r", stdin);
	freopen("grid.out", "w", stdout);
	int T;
	scanf("%d", &T);
	for (int i = 1; i <= T; i++) {
		printf("%d\n", solve());
	}
}
