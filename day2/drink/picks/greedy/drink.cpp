#include <cstdio>
#include <algorithm>
using namespace std;
int n, m, p, h[8010]; double ans;
int main() {
	freopen("drink.in", "r", stdin);
	freopen("drink.out", "w", stdout);
	scanf("%d%d%d", &n, &m, &p); m = min(m, n - 1);
	for (int i = 1; i <= n; i++) scanf("%d", &h[ i ]);
	sort(h + 2, h + n + 1); ans = h[ 1 ];
	for (int i = n - m; i <= n; i++) ans = (ans + h[ i ]) / 2;
	printf("%lf\n", ans);
}
