#include "testlib.h"
#include <algorithm>
#include <stdio.h>
#include <string.h>
#include <assert.h>

const int MIN_N = 1;
const int MAX_N = 8000;

const int MIN_K = 0;
const int MAX_K = 1000000000;

const int MIN_P = 1;
const int MAX_P = 3000;

const int MIN_H = 1;
const int MAX_H = 100000;

int testcase;

int n, k, p;

void check_n() {
	assert(1 <= testcase && testcase <= 20);
	
	if (testcase <= 1) {
		ensuref(n <= 2, "n <= 2");
	} else if (testcase <= 2) {
		ensuref(n <= 4, "n <= 4");
	} else if (testcase <= 3) {
		ensuref(n <= 4, "n <= 4");
	} else if (testcase <= 7) {
		ensuref(n <= 10, "n <= 10");
	} else if (testcase <= 12) {
		ensuref(n <= 100, "n <= 100");
	} else if (testcase <= 13) {
		ensuref(n <= 250, "n <= 250");
	} else if (testcase <= 14) {
		ensuref(n <= 500, "n <= 500");
	} else if (testcase <= 17) {
		ensuref(n <= 700, "n <= 700");
	} else if (testcase <= 18) {
		ensuref(n <= 2500, "n <= 2500");
	} else if (testcase <= 19) {
		ensuref(n <= 4000, "n <= 4000");
	} else if (testcase <= 20) {
		ensuref(n <= 8000, "n <= 8000");
	}
}

void check_k() {
	assert(1 <= testcase && testcase <= 20);
	
	if (testcase <= 3) {
		ensuref(k <= 5, "k <= 5");
	} else if (testcase <= 4) {
		ensuref(k == 1, "k = 1");
	} else if (testcase <= 5) {
		ensuref(k == 1000000000, "k = 10^9");
	} else if (testcase <= 7) {
		ensuref(k <= 10, "k <= 10");
	} else if (testcase <= 8) {
		ensuref(k == 1, "k = 1");
	} else if (testcase <= 9) {
		ensuref(k == 1000000000, "k = 10^9");
	} else if (testcase <= 20) {
		ensuref(k <= 1000000000, "k <= 10^9");
	}
}

void check_p() {
	assert(1 <= testcase && testcase <= 20);
	
	if (testcase <= 8) {
		ensuref(p == 5, "p = 5");
	} else if (testcase <= 12) {
		ensuref(p == 40, "p = 40");
	} else if (testcase <= 13) {
		ensuref(p == 100, "p = 100");
	} else if (testcase <= 14) {
		ensuref(p == 200, "p = 200");
	} else if (testcase <= 17) {
		ensuref(p == 300, "p = 300");
	} else if (testcase <= 18) {
		ensuref(p == 1000, "p = 1000");
	} else if (testcase <= 19) {
		ensuref(p == 1500, "p = 1500");
	} else {
		ensuref(p == 3000, "p = 3000");
	}
}

int main(int argc, char **argv) {
	
	if (argc - 1 != 1) {
		fprintf(stderr, "usage: %s <test_case_id>\n", argv[0]);
		return 1;
	}
	
	registerValidation();
	
	ensuref(sscanf(argv[1], "%d", &testcase) == 1, " ");
	ensuref(1 <= testcase && testcase <= 20, " ");
	
	n = inf.readInt(MIN_N, MAX_N, "n");
	inf.readSpace();
	
	k = inf.readInt(MIN_K, MAX_K, "k");
	inf.readSpace();
	
	p = inf.readInt(MIN_P, MAX_P, "p");
	inf.readEoln();
	
	check_n();
	check_k();
	check_p();
	
	static bool used[MAX_H + 1];
	memset(used, 0, sizeof(used));
	
	for (int i = 1; i <= n; i++) {
		int h = inf.readInt(MIN_H, MAX_H, "h");
		ensuref(used[h] == false, "h_i must be distinct");
		used[h] = true;
		if (i < n) {
			inf.readSpace();
		} else {
			inf.readEoln();
		}
	}
	
	inf.readEof();
	
	return 0;
}
