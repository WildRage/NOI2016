#include "testlib.h"
#include <algorithm>
#include <stdio.h>
#include <string.h>

const int MIN_N = 1;
const int MAX_N = 5000;

// TODO: Ask Picks for the min_k's value
const int MIN_K = 0;
const int MAX_K = 1000000000;

// TODO: Data range may be not final
const int MIN_P = 1;
const int MAX_P = 2000;

const int MIN_H = 1;
const int MAX_H = 100000;

int main(int argc, char **argv) {
	
	if (argc - 1 != 1) {
		fprintf(stderr, "usage: %s <test_case_id>\n", argv[0]);
		return 1;
	}
	
	registerValidation();
	
	int n, k, p;
	n = inf.readInt(MIN_N, MAX_N, "n");
	inf.readSpace();
	
	k = inf.readInt(MIN_K, MAX_K, "k");
	inf.readSpace();
	
	p = inf.readInt(MIN_P, MAX_P, "p");
	inf.readEoln();
	
	static bool used[MAX_H + 1];
	memset(used, 0, sizeof(used));
	
	for (int i = 1; i <= n; i++) {
		int h = inf.readInt(MIN_H, MAX_H, "h");
		ensuref(used[h] == false, "h_i must be distinct");
		used[h] = true;
		if (i < n) {
			inf.readSpace();
		} else {
			inf.readEoln();
		}
	}
	
	inf.readEof();
	
	return 0;
}
