#include <cstdio>
#include <iostream>
#include <algorithm>
using namespace std;

class SegmentTree {
  #define add(v,k) v->ans += k,v->flag += k
private:  
  class Node {
    public:
    int l,r;
    Node *cl,*cr;
    int ans;
    int flag;
    Node(int _l,int _r) {
      l = _l,
      r = _r,
      ans = 0,
      flag = 0;
      cl = cr = NULL;
    }
  } *root;
  
  void insert(Node *u,int l,int r,int c) {
    if (l >= u->r || r <= u->l) return;
    if (l <= u->l && u->r <= r) {
      add(u,c);
      return;
    }
    if (u->cl == NULL) {
      u->cl = new Node(u->l,(u->l + u->r) / 2);
      u->cr = new Node((u->l + u->r) / 2,u->r);
    }
    if (u->flag != 0) {
      add(u->cl,u->flag);
      add(u->cr,u->flag);
      u->flag = 0;
    }
    insert(u->cl,l,r,c);
    insert(u->cr,l,r,c);
    u->ans = max(u->cl->ans,u->cr->ans);
  }
public:
  void insert(int l,int r,int c) {
    insert(root,l,r,c);
  }
  int query_max() {
    return root->ans;
  }
  SegmentTree() {
    root = new Node(0,1000000005);
  }
};

struct segment {
  int l,r;
} S[500100];

bool cpr(segment a,segment b) {
  return (a.r - a.l) < (b.r - b.l);
}

int n,m;

int main() {
  freopen("interval.in","r",stdin);
  freopen("interval.out","w",stdout);
  scanf("%d%d",&n,&m);
  SegmentTree ST;
  for (int i = 0;i < n;i++) {
    scanf("%d%d",&S[i].l,&S[i].r);
  }
  sort(S,S + n,cpr);
  int ans = 2147483647;
  int j = 0;
  for (int i = 0;i < n;i++) {
    ST.insert(S[i].l,S[i].r,1);
    while (ST.query_max() >= m) {
      ans = min(ans,(S[i].r - S[i].l) - (S[j].r - S[j].l));
      ST.insert(S[j].l,S[j].r,-1);
      j++;
      while (S[j].r - S[j].l == S[j - 1].r - S[j - 1].l) {
        ST.insert(S[j].l,S[j].r,-1);
        j++;
      }
    }
  }
  if (ans == 2147483647) ans = -1;
  printf("%d\n",ans);
  return 0;
}