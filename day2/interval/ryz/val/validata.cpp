#include <cstdio>
#include <string>
using namespace std;

const int caseN[] = {20,20,199,200,1000,2000,199,200,200,1999,2000,2000,30000,40000,50000,100000,200000,300000,400000,500000};
const int caseM[] = {9,10,3,3,2,2,60,50,50,500,400,500,2000,1000,15000,20000,20000,50000,90000,200000};
const int maxLR[] = {100,100,100000,100000,100000,100000,5000,5000,1000000000,5000,5000,1000000000,100000,100000,100000,100000,1000000000,1000000000,1000000000,1000000000};

bool error;

void data_assert(bool b) {
  if (!b) {
    puts("assert error");
    error = 1;
  }
}

int check(int num) {
  printf("Now checking test case %d\n",num);
  char buf[10],c;
  int n,m,l,r;
  sprintf(buf,"%d",num);
  string fn = "interval";
  fn += buf;
  fn += ".in";
  FILE *in = fopen(fn.c_str(),"rb");
  
  fscanf(in,"%d%d",&n,&m);
  data_assert(n == caseN[num - 1]);
  data_assert(m == caseM[num - 1]);
  data_assert(1 <= m);
  data_assert(m <= n);
  for (int i = 0;i < n;i++) {
    int ret = fscanf(in,"%d%d",&l,&r);
    data_assert(ret == 2);
    data_assert(l <= r);
    data_assert(r <= maxLR[num - 1]);
  }
  c = fgetc(in);
  data_assert(c == '\n');
  c = fgetc(in);
  data_assert(c == EOF);
}

int main() {
  for (int i = 1;i <= 20;i++) check(i);
  if (!error) puts("tests pass");
  return 0;
}
