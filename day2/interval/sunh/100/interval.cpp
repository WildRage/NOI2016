#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <algorithm>

using namespace std;

#define l first
#define r second
#define pb push_back
#define mp make_pair

typedef pair<int, int> pii;
const int N = 1000200;
char buffer[50], *ch;
inline void read(int &x) {
	x = 0; 
	while ((*ch) < '0' || (*ch) > '9')
		ch++;
	while ((*ch) >= '0' && (*ch) <= '9') {
		x = x * 10;
		x += (*ch) - '0';
		ch++;
	}
}
struct Data {
	int n, m;
	pii a[N];
	void input() {
		gets(buffer);
		ch = buffer;
		read(n);
		read(m);
		for (int i = 1; i <= n; i++) {
			gets(buffer);
			ch = buffer;
			read(a[i].l);
			read(a[i].r);
		}
	}
};
inline bool cmpByLen(const pii &t1, const pii &t2) {
	return (t1.r - t1.l) < (t2.r - t2.l);
}
struct node {
	int l, r;
	int mx, add;
	node *c[2];
} mem[N * 4], *cur;
vector<int> slx;
node *maketree(int l, int r) {
	node *p = cur++;
	p->l = l;
	p->r = r;
	p->add = p->mx = 0;
	int mid = (l + r) >> 1;
	if (l == r) {
		p->c[0] = p->c[1] = NULL;
	} else {
		p->c[0] = maketree(l, mid);
		p->c[1] = maketree(mid + 1, r);
	}
	return p;
}
inline int find(int x) {
	return lower_bound(slx.begin(), slx.end(), x) - slx.begin() + 1;
}
inline void pushdown(node *x) {
	if (x->add) {
		x->c[0]->mx += x->add;
		x->c[1]->mx += x->add;
		x->c[0]->add += x->add;
		x->c[1]->add += x->add;
		x->add = 0;
	}
}
inline void update(node *x) {
	x->mx = max(x->c[0]->mx, x->c[1]->mx);
}
inline void add(node *x, int l, int r, int c) {
	if (l <= x->l && x->r <= r) {
		x->mx += c;
		x->add += c;
		return;
	}
	pushdown(x);
	int mid = (x->l + x->r) >> 1;
	if (l <= mid)
		add(x->c[0], l, r, c);
	if (r > mid)
		add(x->c[1], l, r, c);
	update(x);
}
int solve100(Data *data_) {
	int i, j;
	Data *data = new Data();
	data->n = data_->n;
	data->m = data_->m;
	int n = data->n, m = data->m;
	for (i = 1; i <= data->n; i++) {
		data->a[i] = data_->a[i];
	}
	sort(data->a + 1, data->a + 1 + n, cmpByLen);
	pii *a = data->a;
	slx.clear();
	for (i = 1; i <= n; i++) {
		slx.pb(a[i].l);
		slx.pb(a[i].r);
	}
	sort(slx.begin(), slx.end());
	cur = mem;
	node *root = maketree(1, slx.size());
	const int inf = 0x3f3f3f3f;
	int ans = inf;
	j = 1;
	for (i = 1; i <= n; i++) {
		while (j <= n && root->mx < m) {
			add(root, find(a[j].l), find(a[j].r), 1);
			j++;
		}
		if (root->mx >= m) {
			if (j - 1 < i)
				cerr << "err!" << endl;
			ans = min(ans, (a[j - 1].r - a[j - 1].l) - (a[i].r - a[i].l));
		}
		add(root, find(a[i].l), find(a[i].r), -1);
	}
	if (ans == inf)
		ans = -1;
	//delete data;
	return ans;
}
int main() {
//	makecases();
	freopen("interval.in", "r", stdin);
	freopen("interval.out", "w", stdout);
	Data *data = new Data();
	data->input();
	printf("%d\n", solve100(data));
	delete data;
	fclose(stdin);
	fclose(stdout);
	return 0;
}
