#include <cstdio>
#include <algorithm>

#define l first
#define r second
#define pb push_back
#define mp make_pair

using namespace std;

const int N = 1000200, inf = 0x3f3f3f3f;
typedef pair<int, int> pii;
int n, m;
pii a[N];
int ans = inf;
void dfs(int last, int s, int nowl, int nowr, int minlen, int maxlen) {
	if (s == m) {
		if (nowl <= nowr) {
			ans = min(ans, maxlen - minlen);
		}
		return;
	}
	for (int i = last + 1; i <= n; i++) {
		dfs(i, s + 1, max(nowl, a[i].l), min(nowr, a[i].r), 
			min(minlen, a[i].r - a[i].l), max(maxlen, a[i].r - a[i].l));
	}
}
int main() {
	int i, j;
	freopen("interval.in", "r", stdin);
	freopen("interval.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (i = 1; i <= n; i++)
		scanf("%d%d", &a[i].l, &a[i].r);
	dfs(0, 0, 0, inf, inf, -inf);
	if (ans == inf)
		ans = -1;
	printf("%d\n", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;

}