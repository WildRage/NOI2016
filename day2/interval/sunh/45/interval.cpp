#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <algorithm>

using namespace std;

#define l first
#define r second
#define pb push_back
#define mp make_pair

typedef pair<int, int> pii;
const int N = 1000200;
struct Data {
	int n, m;
	pii a[N];
	void input() {
		scanf("%d%d", &n, &m);
		for (int i = 1; i <= n; i++) {
			scanf("%d%d", &a[i].l, &a[i].r);
		}
	}
};
bool cmpByLen(const pii &t1, const pii &t2) {
	return (t1.r - t1.l) < (t2.r - t2.l);
}
vector<int> slx;
int flag[N * 2];
inline int find(int x) {
	return lower_bound(slx.begin(), slx.end(), x) - slx.begin() + 1;
}
bool color(int l, int r, int m) {
	int i;
	for (i = l; i <= r; i++) {
		flag[i]++;
		if (flag[i] >= m)
			return true;
	}
	return false;
}
int solvebf() {
	Data *data = new Data();
	data->input();
	sort(data->a + 1, data->a + 1 + data->n, cmpByLen);
	int n = data->n, m = data->m;
	pii *a = data->a;
	int i, j;
	slx.clear();
	for (i = 1; i <= n; i++) {
		slx.pb(a[i].l);
		slx.pb(a[i].r);
	}
	sort(slx.begin(), slx.end());
	slx.erase(unique(slx.begin(), slx.end()), slx.end());
	int ans = 1 << 30;
	for (i = 1; i <= n; i++) {
		// cerr << i << endl;
		for (j = 1; j <= slx.size(); j++)
			flag[j] = 0;
		for (j = i; j <= n; j++) {
			if (color(find(a[j].l), find(a[j].r), m)) {
				ans = min(ans, (a[j].r - a[j].l) - (a[i].r - a[i].l));
				break;
			}
		}
	}
	if (ans == (1 << 30))
		ans = -1;
	delete data;
	return ans;
}
int main() {
//	makecases();
	freopen("interval.in", "r", stdin);
	freopen("interval.out", "w", stdout);
	printf("%d\n", solvebf());
	fclose(stdin);
	fclose(stdout);
}
