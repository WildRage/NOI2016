#include "testlib.h"
#include <algorithm>
#include <stdio.h>
#include <assert.h>

const int MIN_N = 1;
const int MAX_N = 500000;

const int MIN_X = 0;
const int MAX_X = 1000000000;

int testcase;

int n, m;

void check_n() {
	assert(1 <= testcase && testcase <= 20);
	
	if (testcase <= 2) {
		ensuref(1 <= n && n <= 20, "1 <= n <= 20");
	} else if (testcase <= 4) {
		ensuref(1 <= n && n <= 200, "1 <= n <= 200");
	} else if (testcase <= 6) {
		ensuref(1 <= n && n <= 2000, "1 <= n <= 2000");
	} else if (testcase <= 9) {
		ensuref(1 <= n && n <= 200, "1 <= n <= 200");
	} else if (testcase <= 12) {
		ensuref(1 <= n && n <= 2000, "1 <= n <= 2000");
	} else if (testcase <= 16) {
		ensuref(1 <= n && n <= 100000, "1 <= n <= 100000");
	} else {
		ensuref(1 <= n && n <= 500000, "1 <= n <= 500000");
	}
}

void check_m() {
	assert(1 <= testcase && testcase <= 20);
	
	if (testcase <= 2) {
		ensuref(1 <= m && m <= 10, "1 <= m <= 10");
	} else if (testcase <= 4) {
		ensuref(m == 3, "m = 3");
	} else if (testcase <= 6) {
		ensuref(m == 2, "m = 2");
	} else {
		ensuref(1 <= m && m <= n, "1 <= m <= n");
	}
}

void check_lr(int l, int r) {
	assert(1 <= testcase && testcase <= 20);
	
	if (testcase <= 2) {
		ensuref(0 <= l && l <= r && r <= 100, "0 <= l <= r <= 100");
	} else if (testcase <= 6) {
		ensuref(0 <= l && l <= r && r <= 100000, "0 <= l <= r <= 100000");
	} else if (testcase <= 8) {
		ensuref(0 <= l && l <= r && r <= 5000, "0 <= l <= r <= 5000");
	} else if (testcase <= 9)  {
		ensuref(0 <= l && l <= r && r <= 1000000000, "0 <= l <= r <= 1000000000");
	} else if (testcase <= 11) {
		ensuref(0 <= l && l <= r && r <= 5000, "0 <= l <= r <= 5000");
	} else if (testcase <= 12) {
		ensuref(0 <= l && l <= r && r <= 1000000000, "0 <= l <= r <= 1000000000");
	} else if (testcase <= 16) {
		ensuref(0 <= l && l <= r && r <= 100000, "0 <= l <= r <= 100000");
	} else {
		ensuref(0 <= l && l <= r && r <= 1000000000, "0 <= l <= r <= 1000000000");
	}
}

int main(int argc, char **argv) {
	
	if (argc - 1 != 1) {
		fprintf(stderr, "usage: %s <test_case_id>\n", argv[0]);
		return 1;
	}
	
	registerValidation();
	
	ensuref(sscanf(argv[1], "%d", &testcase) == 1, " ");
	ensuref(1 <= testcase && testcase <= 20, " ");
	
	n = inf.readInt(MIN_N, MAX_N, "n");
	inf.readSpace();
	
	m = inf.readInt(MIN_N, n, "m");
	inf.readEoln();
	
	check_n();
	check_m();
	
	for (int i = 1; i <= n; i++) {
		int l, r;
		
		l = inf.readInt(MIN_X, MAX_X, "l");
		inf.readSpace();
		
		r = inf.readInt(MIN_X, MAX_X, "r");
		inf.readEoln();
		
		ensuref(l <= r, "l_i must <= r_i");
		
		check_lr(l, r);
	}
	
	inf.readEof();
	
	return 0;
}
